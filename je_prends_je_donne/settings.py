"""
Django settings for erablitek project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ccq44*@oyq$1h2-ihe&f&)5dfdsfbfx^5!%3dsfsdf!6dsfdsfdsoy0ey5vb'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', '*']

ANONYMOUS_USER_ID = -1
PAYPAL_RECEIVER_EMAIL = "paypal@jeprendsjedonne.com"
PAYPAL_TEST = True
# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions.backends.cached_db',
#   'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'paypal.standard.ipn',
    'authenticate',
    'stdimage',
    'parent_categories',
    'child_categories',
    'items',
    'item_status',
    'user_profile',
    'comments',
    'messaging',
    'transactions',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

AUTH_PROFILE_MODULE = 'user_profile.Profile'

ROOT_URLCONF = 'je_prends_je_donne.urls'

WSGI_APPLICATION = 'je_prends_je_donne.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

from database import *

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Montreal'

USE_I18N = True


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Montreal'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
TEMPLATE_DIRS = (
  os.path.join(BASE_DIR, "templates/"),
)

#STATICFILES_DIRS = (
#    os.path.join(BASE_DIR, "static/"),
#    '/root/je_prends_je_donne/static/',
#)
LOGIN_URL = "/user/login/"
LOGOUT_URL = "/user/logout/"
STATIC_ROOT = os.path.join( BASE_DIR, "static/" )
MEDIA_ROOT = os.path.join( BASE_DIR, "media/" )
MEDIA_URL = "media/"


SESSION_EXPIRE_AT_BROWSER_CLOSE = True
#SESSION_COOKIE_AGE = 3600
