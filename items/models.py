from django.db import models
from django.contrib.auth.models import User
from child_categories.models import ChildCategorie
from stdimage import StdImageField

class Item(models.Model):
    
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User, related_name='user_id', db_column='user_id')
    title = models.CharField(max_length=50)
    description = models.TextField()
    credit = models.IntegerField()
    child_category_id = models.ForeignKey(ChildCategorie, related_name='child_category', db_column="child_category_id")
    status = models.ForeignKey('item_status.ItemStatus', related_name='status', db_column="status_id")
    img1 = StdImageField(upload_to = 'items/', blank = True, variations={'large': (800, 600), 'medium': (320, 240, True),'thumbnail': (200, 150)})
    img2 = StdImageField(upload_to = 'items/', blank = True, variations={'large': (800, 600), 'medium': (320, 240, True),'thumbnail': (200, 150)})
    img3 = StdImageField(upload_to = 'items/', blank = True, variations={'large': (800, 600), 'medium': (320, 240, True),'thumbnail': (200, 150)})
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
        
    class Meta:
        db_table = 'items'

    def __unicode__(self):              # __unicode__ on Python 2
        return self.title
    
class ItemReservation(models.Model):
    id = models.AutoField(primary_key=True)
    taker = models.ForeignKey(User, db_column='taker_id', null = True )
    item = models.ForeignKey(Item, db_column='item_id', related_name='reservations', null = True )
    note = models.TextField( null = True )
    viewed = models.BooleanField( default = False )
    accepted = models.BooleanField( default = None )
    created = models.DateTimeField( auto_now = False, auto_now_add = True )
    modified = models.DateTimeField(auto_now=True)
        
    class Meta:
        db_table = 'items_reservations'

    def __unicode__(self):              # __unicode__ on Python 2
        return self.item
        
class ItemDelivery(models.Model):
    id = models.AutoField(primary_key=True)
    item_reservation = models.ForeignKey(ItemReservation, db_column='item_reservation_id', related_name='delivery', null = True )
    giver_note = models.TextField( null = True )
    giver_conf = models.BooleanField( default = False )
    giver_conf_date = models.DateTimeField( null = True )
    taker_note = models.TextField( null = True )
    taker_obj_rating = models.IntegerField( default = 1 )
    taker_deal_rating = models.IntegerField( default = 1 )
    taker_conf = models.BooleanField( default = False )
    taker_conf_date = models.DateTimeField( null = True )
    created = models.DateTimeField( auto_now = False, auto_now_add = True )
    
    class Meta:
        db_table = 'items_delivery'

    def __unicode__(self):              # __unicode__ on Python 2
        return self.id
        
class RequestAlert(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, db_column='taker_id', null = True )
    item_reservation = models.ForeignKey(ItemReservation, db_column='reservation_id', related_name="request_alert", null = True )
    created = models.DateTimeField( auto_now = False, auto_now_add = True )
    
    class Meta:
        db_table = 'request_alerts'
        
class MyAdAlert(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, db_column='user_id', null = True )
    item = models.ForeignKey(Item, db_column='item_id', related_name="item_alert", null = True )
    created = models.DateTimeField( auto_now = False, auto_now_add = True )
    
    class Meta:
        db_table = 'my_ad_alerts'
        
class Report(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, db_column='user', null = True )
    item = models.ForeignKey(Item, db_column='item', related_name="item_report", null = True )
    reason = models.TextField( null = True )
    status = models.IntegerField( default = 1 )
    created = models.DateTimeField( auto_now = False, auto_now_add = True )
    
    class Meta:
        db_table = 'item_reports'