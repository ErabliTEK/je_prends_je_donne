from django.conf.urls import url

from items import views

urlpatterns = [
    url(r'^user_items/(?P<page>[0-9]+)/$', views.user_items),
    url(r'^user_request/(?P<page>[0-9]+)/$', views.user_request),
    url(r'^page/(?P<page>[0-9]+)/$', views.list_items),
    url(r'^request_alert$', views.request_alert),
    url(r'^my_ad_alert$', views.my_ad_alert),
    url(r'^delete/$', views.delete),
    url(r'^del_img/$', views.del_item_image),
    url(r'^latest/$', views.latest),
    url(r'^cancel_demand/$', views.cancel_demand),
    url(r'^report/$', views.report),
    url(r'^edit_item/(?P<item>[0-9]+)/$', views.add_item, name='edit_item'),
    url(r'^add_item/$', views.add_item, name='add_item'),
    url(r'^details/(?P<item_id>[0-9]+)/$', views.item_details, name='item_details'),
    url(r'^reservation/$', views.reservation, name='reservation'),
    url(r'^delivery/$', views.delivery, name='delivery'),
    url(r'^add_transaction/', views.reservation, name='add_transaction'),
    url(r'^list/(?P<child_id>[0-9]+)/$', views.list_items, name='list_item_child'),
    url(r'^$', views.list_items, name='index'),
]