# -*- coding: utf8 -*-
from django.shortcuts import render, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from comments.views import list_comments
from cakeserializer import CakeSerializer
from items.models import Item, ItemReservation, ItemDelivery, RequestAlert, MyAdAlert, Report
from child_categories.models import ChildCategorie
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from item_status.models import ItemStatus
from user_profile.views import get_profile, Profile, get_user_quote
from parent_categories.models import ParentCategorie
from pilkit.processors import Transpose
from PIL import Image
from transactions.models import Transaction
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.utils import timezone
import datetime
import copy
import os, StringIO
from urlparse import urlparse
import math
import json
import magic
from pudb import set_trace
serializer = CakeSerializer()

#set_trace()

@login_required 
def user_items( request, page = 1 ):
    user_items = Item.objects.filter( user_id = request.user ).exclude( ( Q( modified__lt = datetime.date.today() ) & Q( status = 3 ) ) ).order_by( 'status_id' )
    user_items = user_items.exclude( ( Q( modified__lt = datetime.date.today() ) & Q( status = 3 ) ) )
    items = []
    items_alert = []
    profile = get_profile(request.user.id)
    for item in user_items:
        alert = False
        my_ad_alert = None
        my_ad_alert = MyAdAlert.objects.filter( item = item )
        if my_ad_alert:
            alert = len(my_ad_alert)
            items_alert.append({
                'id': item.id,
                'title': item.title,
                'description': item.description,
                'credit': item.credit,
                'child_category': item.child_category_id,
                'user': item.user_id,
                'first_name':profile['profile_data']['first_name'],
                'last_name':profile['profile_data']['last_name'],
                'username': item.user_id.username,
                'city':profile['profile_data']['city'],
                'district':profile['profile_data']['district'],
                'distance': None,
                'created':item.created,
                'status': item.status,
                'img1': item.img1,
                'img2': item.img2,
                'img3': item.img3,
                'poster_profile': profile,
                'alert': alert,
            })
        else:
            items.append({
                'id': item.id,
                'title': item.title,
                'description': item.description,
                'credit': item.credit,
                'child_category': item.child_category_id,
                'user': item.user_id,
                'first_name':profile['profile_data']['first_name'],
                'last_name':profile['profile_data']['last_name'],
                'username': item.user_id.username,
                'city':profile['profile_data']['city'],
                'district':profile['profile_data']['district'],
                'distance': None,
                'created':item.created,
                'status': item.status,
                'img1': item.img1,
                'img2': item.img2,
                'img3': item.img3,
                'poster_profile': profile,
                'alert': alert,
            })
    page = int(page)
    if not page:
        page = 1
    paginator = Paginator(items, 10)
    try:
        tmp = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        tmp = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        tmp = paginator.page(paginator.num_pages)
    items= tmp
    page_info = {
        'current': page,
        'next': page + 1,
        'previous': page - 1,
        'range': paginator.page_range,
        'has_previous': tmp.has_previous(),
        'has_next': tmp.has_next()
        }
    data = {}
    data['items_alert'] = items_alert
    data['items'] = items
    data['page_info'] = page_info
    return render(request, 'items/list.html', { 'data': data, 'profile': profile, 'type': 'user'})

@login_required 
def user_request( request, page = 1 ):
    items = []
    items_alert = []
    profile = get_profile(request.user.id)
    reservations = ItemReservation.objects.filter( taker_id = request.user )
    for reservation in reservations:
        alert = False
        reservation_alert = reservation.request_alert.filter( user = request.user )
        if reservation_alert:
            alert = len(reservation_alert)
        item = reservation.item
        if item.status.id == 3 and (item.modified.date() < datetime.date.today()):
            print 'MODIFIED: {0} < TOTAY: {1}'.format(item.modified.date(), datetime.date.today())
            item = None
        if item:
            item_profile = get_profile( item.user_id_id )
            if profile:
                if item_profile['profile_data']['latitude'] and item_profile['profile_data']['longitude'] and profile['profile_data']['latitude'] and profile['profile_data']['longitude']:
                     distance = distance_func(
                        item_profile['profile_data']['latitude'],
                        item_profile['profile_data']['longitude'],
                        profile['profile_data']['latitude'],
                        profile['profile_data']['longitude']
                    )
            if reservation_alert:
                items_alert.append({
                    'id': item.id,
                    'title': item.title,
                    'description': item.description,
                    'credit': item.credit,
                    'child_category': item.child_category_id,
                    'user': item.user_id,
                    'first_name': item_profile['profile_data']['first_name'],
                    'last_name': item_profile['profile_data']['last_name'],
                    'username': item.user_id.username,
                    'city': item_profile['profile_data']['city'],
                    'district': item_profile['profile_data']['district'],
                    'distance': distance,
                    'created': item.created,
                    'status': item.status,
                    'img1': item.img1,
                    'img2': item.img2,
                    'img3': item.img3,
                    'poster_profile': profile,
                    'alert': alert,
                })
            else:
                items.append({
                    'id': item.id,
                    'title': item.title,
                    'description': item.description,
                    'credit': item.credit,
                    'child_category': item.child_category_id,
                    'user': item.user_id,
                    'first_name': item_profile['profile_data']['first_name'],
                    'last_name': item_profile['profile_data']['last_name'],
                    'username': item.user_id.username,
                    'city': item_profile['profile_data']['city'],
                    'district': item_profile['profile_data']['district'],
                    'distance': distance,
                    'created': item.created,
                    'status': item.status,
                    'img1': item.img1,
                    'img2': item.img2,
                    'img3': item.img3,
                    'poster_profile': profile,
                    'alert': alert,
                })
    page = int(page)
    if not page:
        page = 1
    paginator = Paginator(items, 10)
    try:
        tmp = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        tmp = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        tmp = paginator.page(paginator.num_pages)
    items= tmp
    page_info = {
        'current': page,
        'next': page + 1,
        'previous': page - 1,
        'range': paginator.page_range,
        'has_previous': tmp.has_previous(),
        'has_next': tmp.has_next()
        }
    data = {}
    data['items_alert'] = items_alert
    data['items'] = items
    data['page_info'] = page_info
    return render(request, 'items/list.html', { 'data': data, 'profile': profile, 'type': 'request'})








def list_items(request, for_user = None, page = 1, child_id = None):
    search = None
    alert = False
    max_distance = 25
    profile = ''
    status = 4
    filter = ''
    if request.GET:
        if 'page' in request.GET:
            page = request.GET['page']
        if 'max_distance' in request.GET:
            max_distance = request.GET['max_distance']
        if 'status' in request.GET:
            status = request.GET['status']
        if 'search' in request.GET:
            search = request.GET['search']
        
    data = {}
    items = []
    user = request.user
    if user.is_anonymous():
        data['logged'] = 0
    else:
        profile = get_profile(request.user.id)
        data['logged'] = 1
        data['first_name'] = user.first_name
        data['last_name'] = user.last_name
    tmp = Item.objects.all().order_by('id').reverse()
    if search:
        tmp = tmp.filter( Q(title__icontains = search) | Q(description__icontains = search) | Q( user_id__username__icontains = search))
    if for_user:
        tmp = tmp.filter( user_id_id = user.id )
    if child_id:
        tmp = tmp.filter( child_category_id_id = child_id )
    if int(status) == 4:
        tmp = tmp.filter( Q( status = 1 ) | Q( status_id = 2 ) )
    elif int(status) != 0:
        tmp = tmp.filter( status_id = status )
    # Here we remove every disabled item before today.
    tmp = tmp.exclude( ( Q( modified__lt = datetime.date.today() ) & ( Q( status = 3 ) or Q( status = 4 ) ) ) )
    
    for x in tmp:
        item_profile = get_profile(x.user_id.id)
        
        description = x.description.strip()
        distance = None
        if profile:
            if item_profile['profile_data']['latitude'] and item_profile['profile_data']['longitude'] and profile['profile_data']['latitude'] and profile['profile_data']['longitude']:
                 distance = distance_func(
                    item_profile['profile_data']['latitude'],
                    item_profile['profile_data']['longitude'],
                    profile['profile_data']['latitude'],
                    profile['profile_data']['longitude']
                )
        skip = False
        if max_distance and distance:
            if int(distance) > int(max_distance):
                skip = True
        if not skip:
            items.append({
                'id': x.id,
                'title': x.title,
                'description': description,
                'credit': x.credit,
                'child_category': x.child_category_id,
                'user': x.user_id,
                'first_name':item_profile['profile_data']['first_name'],
                'last_name':item_profile['profile_data']['last_name'],
                'username': x.user_id.username,
                'city':item_profile['profile_data']['city'],
                'district':item_profile['profile_data']['district'],
                'distance': distance,
                'created':x.created,
                'status': x.status,
                'img1': x.img1,
                'img2': x.img2,
                'img3': x.img3,
                'poster_profile': get_profile(x.id),
                'alert': alert,
            })
    page = int(page)
    if not page:
        page = 1
    paginator = Paginator(items, 25)
    try:
        tmp = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        tmp = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        tmp = paginator.page(paginator.num_pages)
    items= tmp
    page_info = {
        'current': page,
        'next': page + 1,
        'previous': page - 1,
        'range': paginator.page_range,
        'has_previous': tmp.has_previous(),
        'has_next': tmp.has_next()
    }
    data['items'] = items
    data['page_info'] = page_info
    if child_id:
        data['child_categories'] = ChildCategorie.objects.get( pk = child_id )
        data['parent_categories'] = ParentCategorie.objects.get( pk = data['child_categories'].parent_category_id.id )
    return render(request, 'items/list.html', { 'data': data, 'profile': profile, 'max_distance': int(max_distance), 'status': int(status), 'type': 'list', 'search': search})




@login_required   
def add_item(request, item = None):
    form = {}
    profile = get_profile(request.user.id)
    user = request.user
    if user.is_anonymous():
        logged = 0
    else:
        logged = 1
    form = {}
    form['field'] = {}
    form['error'] = {}
    form['field']['image_1'] = {}
    form['field']['image_2'] = {}
    form['field']['image_3'] = {}
    form['field']['user_id'] = {}
    form['field']['status_id'] = {}
    form['field']['parent_category_id'] = {}
    form['field']['parent_category_id']['options'] = []
    form['field']['parent_category_id']['options'] = ParentCategorie.objects.all()
    form['field']['child_category_id'] = {}
    form['field']['title'] = {}
    form['field']['credit'] = {}
    form['field']['description'] = {}
    if item:
        try:
            item = Item.objects.get( id = item )
#            return HttpResponse('<p>{0}</p><br/><p>{1}</p>'.format(item.user_id, request.user))
            if item.user_id.id != request.user.id:
                return HttpResponseRedirect('/')
            print 'ITEM ID: {0}'.format(item.id)
            print 'TEST: {0}'.format(item.child_category_id.parent_category_id.id)
            form['field']['item_id'] = item.id
            form['field']['user_id'] = item.user_id.id
            form['field']['status_id'] = item.status.id
            form['field']['parent_category_id']['value'] = item.child_category_id.parent_category_id.id
            form['field']['child_category_id']['value'] = item.child_category_id.id
            form['field']['title']['value'] = item.title
            form['field']['credit']['value'] = str(item.credit)
            form['field']['description']['value'] = item.description
            form['field']['image_1']['value'] = item.img1
            form['field']['image_2']['value'] = item.img2
            form['field']['image_3']['value'] = item.img3
            print 'Image 1: {0}'.format(form['field']['image_1']['value'])
            print 'Image 2: {0}'.format(form['field']['image_2']['value'])
            print 'Image 3: {0}'.format(form['field']['image_3']['value'])
        except:
            pass
    # A HTTP POST?
    if request.method == 'POST':
        # Have we been provided with a valid form?
        if 'img1' in request.FILES:
            image1 = request.FILES['img1']
            if valid_image_mimetype(image1):
                image1 = Image.open(image1)
                
                output = StringIO.StringIO()
                image1 = Transpose().process(image1)
                image1.save(output, "JPEG")
                image1 = InMemoryUploadedFile(
                    output,
                    None,
                    'foo1.jpg',
                    'image/jpeg',
                    output.len,
                    None
                )
                form['field']['image_1']['value'] = image1
            else:
                form['error']['image_1'] = '<span class="color-red"> Format d\'image invalide </span>'
        else:
            image1 = None
            
        if 'img2' in request.FILES:
            image2 = request.FILES['img2']
            if valid_image_mimetype(image2):
                image2 = Image.open(image2)
                output = StringIO.StringIO()
                image2 = Transpose().process(image2)
                image2.save(output, "JPEG")
                image2 = InMemoryUploadedFile(
                    output,
                    None,
                    'foo2.jpg',
                    'image/jpeg',
                    output.len,
                    None
                )
                form['field']['image_2']['value'] = image2
            else:
                form['error']['image_2'] = '<span class="color-red"> Format d\'image invalide </span>'
        else:
            image2 = None
        if 'img3' in request.FILES:
            image3 = request.FILES['img3']
            if valid_image_mimetype(image3):
                image3 = Image.open(image3)
                output = StringIO.StringIO()
                image3 = Transpose().process(image3)
                image3.save(output, "JPEG")
                image3 = InMemoryUploadedFile(
                    output,
                    None,
                    'foo3.jpg',
                    'image/jpeg',
                    output.len,
                    None
                )
                form['field']['image_3']['value'] = image3
            else:
                form['error']['image_3'] = '<span class="color-red"> Format d\'image invalide </span>'
        else:
            image3 = None
        if 'item_id' in request.POST:
            item_id = request.POST['item_id']
        else:
            item_id = None
        if 'user_id' in request.POST:
            user_id = request.POST['user_id']
            if not user_id:
                form['error']['user_id'] = '<span class="color-red"> Ce champ est requis </span>'
            else:
                form['field']['user_id']['value'] = user_id
                
        if 'status_id' in request.POST:
            status_id = request.POST['status_id']
            if not status_id:
                form['error']['status_id'] = '<span class="color-red"> Ce champ est requis </span>'
            else:
                form['field']['status_id']['value'] = status_id
                
        if 'parent_category_id' in request.POST:
            parent_category_id = request.POST['parent_category_id']
            if not parent_category_id:
                form['error']['parent_category_id'] = '<span class="color-red"> Ce champ est requis </span>'
            else:
                form['field']['parent_category_id']['value'] = int(parent_category_id)
                
        if 'child_category_id' in request.POST:
            child_category_id = request.POST['child_category_id']
            if not child_category_id:
                form['error']['child_category_id'] = '<span class="color-red"> Ce champ est requis </span>'
            else:
                form['field']['child_category_id']['value'] = int(child_category_id)
        
        if 'title' in request.POST:
            title = request.POST['title']
            if not title:
                form['error']['title'] = '<span class="color-red"> Ce champ est requis </span>'
            else:
                form['field']['title']['value'] = title

        if 'credit' in request.POST:
            credit = request.POST['credit']
            if not credit:
                form['error']['credit'] = '<span class="color-red"> Ce champ est requis </span>'
            else:
                form['field']['credit']['value'] = credit
                
        if 'description' in request.POST:
            description = request.POST['description']
            description = description.lstrip().rstrip()
            if not description:
                form['error']['description'] = '<span class="color-red"> Ce champ est requis </span>'
            else:
                form['field']['description']['value'] = description
        
        if not form['error']:
            if not item_id:
                new_item = Item(
                    user_id = User.objects.get( id = user_id ),
                    title = title,
                    description = description,
                    child_category_id = ChildCategorie.objects.get( id = child_category_id ),
                    credit = credit,
                    status = ItemStatus.objects.get( id = status_id ))
                print 'IS THERE ANY IMAGE?: IMAGE1: {0}, IMAGE2: {1}, IMAGE3: {2}'.format(image1, image2, image3)
                if image1:
                    new_item.img1 = image1
                if image2:
                    new_item.img2 = image2
                if image3:
                    new_item.img3 = image3
                new_item.save()
            else:
                item = Item.objects.get( id = item_id )
                item.title = title
                item.description = description
                item.child_category_id = ChildCategorie.objects.get( id = child_category_id )
                item.credit = credit
                item.status = ItemStatus.objects.get( id = status_id )
                if image1:
                    item.img1 = image1
                if image2:
                    item.img2 = image2
                if image3:
                    item.img3 = image3
                item.save()
                new_item = item
                
            # Save the new category to the database.
            # Now call the index() view.
            # The user will be shown the homepage.
            return HttpResponseRedirect( '/items/details/' + str(new_item.id) )
        else:
            return render(request, 'items/add_item.html', {'form': form, 'logged': logged, 'first_name': user.first_name, 'last_name': user.last_name, 'profile':profile})        
    # If the request was not a POST, display the form to enter details.
    return render(request, 'items/add_item.html', {'form': form, 'logged': logged, 'first_name': user.first_name, 'last_name': user.last_name, 'profile':profile})

    
def item_details( request, item_id, message = ''):
    print 'MESSAGE: {0}'.format(message)
    no_edit = False
    last_url = ''
    if 'HTTP_REFERER' in request.META:
        url = urlparse(request.META['HTTP_REFERER'])
        last_url = url.path + '?' + url.query
    item = None
    distance = None
    poster_profile = None
    data = {}
    if request:
        profile = get_profile(request.user.id)
        user = request.user
        if user.is_anonymous():
            data['logged'] = 0
        else:
            data['logged'] = 1
            data['first_name'] = user.first_name
            data['last_name'] = user.last_name
    if item_id:
        item = Item.objects.get( pk = item_id )
        item_profile = get_profile(item.user_id.id)
        if profile:
            if item_profile['profile_data']['latitude'] and item_profile['profile_data']['longitude'] and profile['profile_data']['latitude'] and profile['profile_data']['longitude']:
                distance = distance_func(
                    item_profile['profile_data']['latitude'],
                    item_profile['profile_data']['longitude'],
                    profile['profile_data']['latitude'],
                    profile['profile_data']['longitude']
                )
        #On delete les alertes sur item.
        if request.user.id:
            MyAdAlert.objects.filter( Q(user = request.user) & Q(item = item) ).delete()
        reservations = item.reservations.all().order_by('id')
        reservations_list = []
        delivery_dict = {}
        reservation_request = False
        for reservation in reservations:
            if reservation.taker == request.user:
                reservation_request = True
                #On Delete les RequestAlert pour cet item pour l'utilisateur en cour.
                if request.user.id:
                    request_alert = reservation.request_alert.filter( user = request.user ).delete()
            if reservation.accepted:
                delivery = ItemDelivery.objects.get( item_reservation = reservation )
                no_edit = True
                delivery_dict = {
                    'giver_conf': delivery.giver_conf,
                    'giver_conf_date': delivery.giver_conf_date,
                    'taker_conf': delivery.taker_conf,
                    'taker_conf_date': delivery.taker_conf_date,
                    'taker_obj_rating': delivery.taker_obj_rating,
                    'taker_deal_rating': delivery.taker_deal_rating,
                    'taker_note': delivery.taker_note,
                    'created': delivery.created,
                }
            reservations_list.append(
                {
                    'id': reservation.id,
                    'user': reservation.taker.id,
                    'username': reservation.taker.username,
                    'first_name': reservation.taker.first_name,
                    'last_name': reservation.taker.last_name,
                    'city': reservation.taker.profile.city,
                    'prov_state': reservation.taker.profile.prov_state, 
                    'avaible_credits': reservation.taker.profile.avaible_credits,
                    'note': reservation.note,
                    'viewed': reservation.viewed,
                    'accepted': reservation.accepted,
                    'created': reservation.created,
                    'delivery': delivery_dict,
                }
            )
            reservation.viewed = True
            reservation.save()
            
        comments = item.comments.all().order_by('id')
        comments_list = []
        for x in comments:            
            comments_list.append(
                {
                    'first_name': x.user_id.first_name,
                    'last_name': x.user_id.last_name,
                    'city': x.user_id.profile.city,
                    'prov_state': x.user_id.profile.prov_state,
                    'comment': x.comment,
                    'viewed': x.viewed,
                    'created': x.created,
                }
            )
        poster_profile = get_profile( item.user_id.id )
        #On verifie la quote de l'utilisateur qui a poster l'item
        poster_profile['rating'] = get_user_quote(item.user_id)
        print 'MESSAGE BEFORE SENDING: {0}'.format(message)
        return render(
            request,
            'items/item_details.html',
            {
                'item': item,
                'no_edit': no_edit,
                'distance': distance,
                'data':data,
                'profile': profile,
                'poster_profile': poster_profile,
                'comments': comments,
                'reservations': reservations_list,
                'last_url': last_url,
                'reservation_request': reservation_request,
                'message': message
            }
        )
    return HttpResponseRedirect( '/items/' )
    
    
@login_required     
def reservation(request):
    item_id = None
    if request.POST:
        item = Item.objects.get( id = request.POST['item_id'] )
        item_id = item.id
        if 'note' in request.POST:
            note = request.POST['note']
        else:
            note =''
        new_reservation = ItemReservation( taker = request.user, item = item, note = note, accepted = False)
        new_reservation.save()
        my_ad_alert = MyAdAlert( user = item.user_id, item = item )
        my_ad_alert.save()
    if request.GET:
        item = Item.objects.get( id = request.GET['item_id'] )
        item_id = item.id
        reservation = ItemReservation.objects.get( item = item, taker = request.GET['taker'] )
        if request.GET['status'] == 'accept':
            reservation.accepted = True
            item.status_id = 2
            item.save()
            # On fait le transfert de fond dans la partie non disponible
            taker_profile = Profile.objects.get( user_id = reservation.taker )
            taker_profile.avaible_credits -= item.credit
            taker_profile.unavaible_credits += item.credit
            taker_profile.save()
            giver_profile = Profile.objects.get( user_id = item.user_id )
            giver_profile.unavaible_credits += item.credit
            giver_profile.save()
            request_alert = RequestAlert( user = reservation.taker, item_reservation = reservation )
            request_alert.save()
            
            # On ajoute une transaction pour nos log!
            taker_transaction = Transaction(
                user = reservation.taker,
                item = item,
                reservation = reservation,
                credit = item.credit,
                total_credit = taker_profile.avaible_credits,
                unavaible_credits = taker_profile.unavaible_credits,
                total_transaction = taker_profile.transactions,
                type = 'Reservé'
            )
            taker_transaction.save()
            
            giver_transaction = Transaction(
                user = item.user_id,
                item = item,
                reservation = reservation,
                credit = item.credit,
                total_credit = giver_profile.avaible_credits,
                unavaible_credits = giver_profile.unavaible_credits,
                total_transaction = giver_profile.transactions,
                type = 'Accepté'
            )
            giver_transaction.save()
            
        elif request.GET['status'] == 'refuse':
            reservation.accepted = False
        reservation.save()
        delivery = ItemDelivery( item_reservation = reservation )
        delivery.save()
    return HttpResponseRedirect( '/items/details/' + str(item_id) )

@login_required 
def delivery(request):
    delivery = None
    form_error = {}
    form_data = {}
    if request.POST:
        print request.POST
        if request.POST['from'] == 'taker':
            if 'item_id' in request.POST:
                form_data['item_id'] = request.POST['item_id']
            if 'reservation_id' in request.POST:
                form_data['reservation'] = ItemReservation.objects.get( id = request.POST['reservation_id'] )
            if 'confirmation' in request.POST:
                form_data['confirmation'] = True
            else:
                form_error['confirmation'] = True
                
            if 'note' in request.POST:
                form_data['note'] = request.POST['note']
                
            if 'object-rating' in request.POST:
                form_data['object_rating'] = request.POST['object-rating']
                
            if 'deal-rating' in request.POST:
                form_data['deal_rating'] = request.POST['deal-rating']
                
            if not form_error:
                delivery = ItemDelivery.objects.get( item_reservation = form_data['reservation'] )
                delivery.taker_note = form_data['note']
                delivery.taker_conf = True
                delivery.taker_conf_date = timezone.make_aware(datetime.datetime.now(),timezone.get_default_timezone())
                delivery.taker_obj_rating = form_data['object_rating']
                delivery.taker_deal_rating = form_data['deal_rating']
                delivery.save()
                my_ad_alert = MyAdAlert( user = form_data['reservation'].item.user_id, item = form_data['reservation'].item )
                my_ad_alert.save()
                
        elif request.POST['from'] == 'giver':
            if 'item_id' in request.POST:
                form_data['item_id'] = request.POST['item_id']
            if 'reservation_id' in request.POST:
                form_data['reservation'] = ItemReservation.objects.get( id = request.POST['reservation_id'] )
            if 'confirmation' in request.POST:
                form_data['confirmation'] = True
            else:
                form_error['confirmation'] = True
            
            if not form_error:
                delivery = ItemDelivery.objects.get( item_reservation = form_data['reservation'] )
                delivery.giver_conf = True
                delivery.giver_conf_date = timezone.make_aware(datetime.datetime.now(),timezone.get_default_timezone())
                delivery.save()
                request_alert = RequestAlert( user = form_data['reservation'].taker, item_reservation = form_data['reservation'] )
                request_alert.save()

        if delivery:
            if delivery.giver_conf and delivery.taker_conf:
                
                item = Item.objects.get( id = form_data['item_id'] )
                item.status_id = 3
                item.save()
                reservation = ItemReservation.objects.get( id = form_data['reservation'].id )
                # On fait le transfert de fond dans la artie non disponible
                taker_profile = Profile.objects.get( user_id = reservation.taker )
                taker_profile.unavaible_credits -= item.credit
                taker_profile.transactions -= 1
                taker_profile.save()
                giver_profile = Profile.objects.get( user_id = item.user_id )
                giver_profile.avaible_credits += item.credit
                giver_profile.save()
                
                # On ajoute une transaction pour nos log!
                taker_transaction = Transaction(
                    user = reservation.taker,
                    item = item,
                    delivery = delivery,
                    credit = item.credit,
                    total_credit = taker_profile.avaible_credits,
                    unavaible_credits = taker_profile.unavaible_credits,
                    transaction = 1,
                    total_transaction = taker_profile.transactions,
                    type = 'Reçu'
                )
                taker_transaction.save()
                
                giver_transaction = Transaction(
                    user = item.user_id,
                    item = item,
                    delivery = delivery,
                    credit = item.credit,
                    total_credit = giver_profile.avaible_credits,
                    unavaible_credits = giver_profile.unavaible_credits,
                    total_transaction = giver_profile.transactions,
                    type = 'Donné'
                )
                giver_transaction.save()
                
#    print 'FORM ERROR: {0}'.format(form_error)
#    return item_details( request, form_data['item_id'], form_error)
    return HttpResponseRedirect(
        '/items/details/' + str(form_data['item_id'])
    )

@login_required 
def delete(request):
    if request.method == 'POST':
        item_id = request.POST['item_id']
        item = Item.objects.get( id = item_id )
        if item.status_id == 2:
            reservations = ItemReservation.objects.filter( item = item )
            reservation = reservations.get( accepted = 1)
            taker_profile = Profile.objects.get( user_id = reservation.taker )
            taker_profile.avaible_credits += item.credit
            taker_profile.unavaible_credits -= item.credit
            taker_profile.save()
            giver_profile = Profile.objects.get( user_id = item.user_id )
            giver_profile.unavaible_credits -= item.credit
            giver_profile.save()
            # On ajoute une transaction pour nos log!
            taker_transaction = Transaction(
                user = reservation.taker,
                item = item,
                reservation = reservation,
                credit = item.credit,
                total_credit = taker_profile.avaible_credits,
                unavaible_credits = taker_profile.unavaible_credits,
                total_transaction = taker_profile.transactions,
                type = 'Suprimé'
            )
            taker_transaction.save()
            
            giver_transaction = Transaction(
                user = item.user_id,
                item = item,
                reservation = reservation,
                credit = item.credit,
                total_credit = giver_profile.avaible_credits,
                unavaible_credits = giver_profile.unavaible_credits,
                total_transaction = giver_profile.transactions,
                type = 'Annonce Suprimée'
            )
            giver_transaction.save()
        item.status_id = 4
        item.save()
    return HttpResponseRedirect('/')

@login_required 
def report(request):
    if request.method == 'POST':
        item = Item.objects.get( id = request.POST['item_id'])
        user = User.objects.get( id = request.POST['user_id'])
        reason = request.POST['reason']
        report = Report(
            user = user,
            item = item,
            reason = reason
        )
        report.save()
        emails = []
        admins = User.objects.filter( is_staff = 1 )
        for admin in admins:
            emails.append(admin.email)
        
        message = u'Vous avez reçu une alerte à propos de l\'annonce: {0}.\n'.format(item.title)
        message += u'Cette alerte a été envoyée par: {0} {1}.\n'.format(user.first_name, user.last_name)
        message += u'Voici la note qu\'il/elle a laissée:\n\n'
        message += u'{0}\n'.format(reason)
        message += u'http://jeprendsjedonne.com/items/details/{0}'.format(item.id)
        result = send_mail(
            u'Vous avez reçu une alerte a propos de l\'item # {0}'.format(item.id),
            message,
            'alert@jeprendsjedonne.com',
            emails,
            fail_silently=False
        )
        print 'SEND MAIL RESULT: {0}'.format(result)
        return HttpResponseRedirect('/items/details/{0}'.format(item.id))
    return HttpResponseRedirect('/')

def cancel_demand(request):
    if request.method == 'POST':
        try:
            reservation = ItemReservation.objects.get( Q( taker = request.user ) & Q( item_id = request.POST['item_id'] ) )
        except:
            reservation = None
        if reservation:
            reservation.delete()
    return HttpResponseRedirect('/items/details/' + str(request.POST['item_id']))
    
def my_ad_alert(request):
    my_ad_alerts = 0
    try:
        my_ad_alerts = MyAdAlert.objects.filter( user = request.user )
        alert_num = my_ad_alerts.count()
    except:
        alert_num = 0
    if alert_num == 0:
        return HttpResponse( 0 )
    for ad in my_ad_alerts:
        if ad.item.status.id == 3:
            ad.delete()
    alert_num = my_ad_alerts.count()
    return HttpResponse( alert_num )
        
def request_alert(request):
    request_alerts = 0
    try:
        request_alerts = RequestAlert.objects.filter( user = request.user )
        request_num = request_alerts.count()
    except:
        request_num = 0
    if  request_num == 0:
        return HttpResponse( 0 )
    for request in request_alerts:
        print request.id
        if request.item_reservation.item.status.id == 3:
            request.delete()
    request_num = request_alerts.count()
    return HttpResponse( request_num )

def del_item_image(request):
    item_id = None
    img_id = None
    if 'item' in request.GET and 'img' in request.GET:
        item_id = int(request.GET['item'])
        img_id = int(request.GET['img'])
    else:
        return HttpResponseRedirect('/items/')
    item = Item.objects.get(id=item_id)
    print item.img2
    if img_id == 1:
        item.img1 = None
    elif img_id == 2:
        item.img2 = None
    elif img_id == 3:
        item.img3 = None
    item.save()
    print item.img2
    return HttpResponseRedirect('/items/edit_item/{0}/'.format(item_id))
    
    
def latest(request):
    items = Item.objects.filter( status_id = 1 ).order_by('id').reverse()[:5]
    print items.count()
    response = ''
    for item in items:
        response += u'  <li><a href="/items/details/{0}">{1}</a></li>'.format(item.id, item.title)
    return HttpResponse(response)

def distance_func(lat1, lon1, lat2, lon2):
    R = 6371000
    x = (math.radians(lon2) - math.radians(lon1)) * math.cos( 0.5*(math.radians(lat2)+math.radians(lat1)) )
    y = math.radians(lat2) - math.radians(lat1)
    d = R * math.sqrt( x*x + y*y )
    return int(d / 1000)

def get_mimetype(fobject):
    mime = magic.Magic(mime=True)
    mimetype = mime.from_buffer(fobject.read(1024))
    fobject.seek(0)
    return mimetype

def valid_image_mimetype(fobject):
    # http://stackoverflow.com/q/20272579/396300
    mimetype = get_mimetype(fobject)
    if mimetype:
        return mimetype.startswith('image')
    else:
        return False 