import pudb
from django import forms    
from parent_categories.models import ParentCategorie

  
class add_item_form(forms.Form):
    parent_categories = []
    obj = ParentCategorie.objects.all()
    for x in obj:
        parent_categories.append((x.id,x.name))
    user_id = forms.IntegerField( widget = forms.HiddenInput( attrs = { 'class':'form-control' } ))
    parent_category_id = forms.IntegerField( required=False, label = 'Categorie Principale:', widget = forms.Select( attrs = { 'class':'form-control' }, choices = parent_categories ))
    child_category_id = forms.IntegerField( required=True, label = 'Categorie Secondaire:', widget = forms.Select( attrs = { 'class':'form-control' }, choices = [(1,'Veuillez selectionner une categorie principale.')] ))
    title = forms.CharField( required=False, label = 'Titre:', widget=forms.TextInput(attrs={'class':'form-control'}))
    credit = forms.CharField( required=False, label = 'Credit:', widget= forms.Select( attrs = { 'class':'form-control' }, choices = [(1, '1'), (2, '2'), (3, '3')] ))
    description = forms.CharField( required=False, label = 'Description:', widget=forms.Textarea(attrs={'class':'form-control'}))
    img1 = forms.FileField( required=False, label = 'Photo 1:', widget=forms.FileInput(attrs={'class':'form-control'}), initial = '')
    img2 = forms.FileField( required=False, label = 'Photo 2:', widget=forms.FileInput(attrs={'class':'form-control'}), initial = '')
    img3 = forms.FileField( required=False, label = 'Photo 3:', widget=forms.FileInput(attrs={'class':'form-control'}), initial = '')
    status_id = forms.IntegerField( widget = forms.HiddenInput( attrs = { 'class':'form-control' }), initial = 1)

  
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(add_item_form, self).__init__(*args, **kwargs)
        self.fields['user_id'].initial = self.user.id