from django.conf.urls import url
from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm
from authenticate import views

urlpatterns = [
    url(r'^login/$', views.login_user),
    url(r'^lost/$', views.lost),
    url(r'^logout/$', views.logout_user),
    url(r'^subscribe/$', views.subscribe),
    url(r'^activate/$', views.activate),
    url(r'^$', views.login_user),
]