# -*- coding: utf8 -*-
from django.shortcuts import render_to_response, render, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from user_profile.models import Profile
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
import hashlib
from lepl.apps.rfc3696 import Email
import os
import string
email_check = Email()

def login_user(request):
    state = "Please log in below..."
    username = ''
    password = ''
    next = '/'
    if request.GET:
        if 'next' in request.GET:
            next = request.GET['next']
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if request.POST.get('next') != None:
            next = request.POST.get('next')
        if user is not None:
            if user.is_active:
                login(request, user)
                state = "You're successfully logged in!"
                try:
                    profile = user.profile
                except:
                    next = '/user_profile'
                return HttpResponseRedirect( next )
            else:
                return render( request, 'authenticate/activate.html' )
        else:
            state = "Your username and/or password were incorrect."

    return render(request, 'authenticate/login.html',{'state':state, 'username': username, 'next': next})
    
def logout_user(request):
    logout(request)
    return HttpResponseRedirect( "/" )
    
def subscribe(request):
#    if request.user.is_authenticated():
#        return HttpResponseRedirect( '/' )
    errors = {}
    data = {}
    username = ''
    first_name = ''
    last_name = ''
    password = ''
    email = ''
    error_message = '<span class="color-red">Champ Requis</span>'
    password_not_match = '<span class="color-red">Non Identique</span>'
    password_too_short = '<span class="color-red">8 Caracteres Requis</span>'
    email_not_valid = '<span class="color-red">Courriel non Valide</span>'
    terms_not_confirmed = '<span class="color-red">Vous devez accepter les termes</span>'
    if request.POST:
        if not request.POST['username']:
            errors['username'] = error_message
        else:
            if user_exit(request.POST['username']):
                errors['username'] = '<span class="color-red">Déjà utilisé</span>'
            else:
                data['username'] = request.POST['username']
                
        if not request.POST['email']:
            errors['email'] = error_message
        else:
            if email_check(request.POST['email']):
                if email_exist(request.POST['email']):
                    errors['email'] = '<span class="color-red">Déjà utilisé</span>'
                else:
                    data['email'] = request.POST['email']
            else:
                errors['email'] = email_not_valid
                
        if not request.POST['first_name']:
            errors['first_name'] = error_message
        else:
            data['first_name'] = request.POST['first_name']
        if not request.POST['last_name']:
            errors['last_name'] = error_message
        else:
            data['last_name'] = request.POST['last_name']
        if not request.POST['password']:
            errors['password'] = error_message
        else:
            data['password'] = request.POST['password']
        if not request.POST['confirm_password']:
            errors['confirm_password'] = error_message
        else:
            data['confirm_password'] = request.POST['confirm_password']
        if 'password' in data and 'confirm_password' in data and data['password'] and data['confirm_password']:
            if data['password'] != data['confirm_password']:
                errors['password'] = password_not_match
                errors['confirm_password'] = password_not_match
            else:
                if len(data['password']) < 8:
                    errors['password'] = password_too_short
  
        if 'terms_confirmed' in request.POST:
            data['terms_confirmed'] = True
        else:
            errors['terms_confirmed'] = terms_not_confirmed
               
        if errors:
            return render(request, 'authenticate/subscribe.html', {'errors':errors, 'data': data})
        
        user = User.objects.create_user(data['username'], data['email'], data['password'])
        user.first_name = data['first_name']
        user.last_name = data['last_name']
        user.is_active = 1
        user.save()
        #email_hash = generate_random_string(40)
        #print 'Trying to send email'
        #mail_sent = send_mail(
        #    'Email Confirmation',
        #    'Veuillez svp confirmer votre courriel en suivant ce liens: http://jeprendsjedonne.com/user_profile/confirm?user={0}&hash={1}'.format(user.id, email_hash),
        #    'martin.chicoine.bt@gmail.com',
        #    [user.email],
        #    fail_silently=False
        #)
        #print 'MAIL SENT: {0}'.format(mail_sent)
        user_profile = Profile(
            user_id = user,
            email_confirm_hash = email_hash,
            avaible_credits = 2,
            transactions = 2,
        )
        user_profile.save()
        user = authenticate(username=data['username'], password=data['password'])
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect( '/user_profile/' )
        else:
            return render( request, 'authenticate/activate.html' )
        
    return render(request, 'authenticate/subscribe.html', {'errors':errors, 'data': data})
    
def activate(request):
    return render( request, 'authenticate/activate.html' )
    
def user_exit(username):
    try:
        if User.objects.filter(username=username).count():
            return True
    
        return False
    except:
        return False
        
def email_exist(email):
    try:
        if User.objects.filter(email=email).count():
            return True
    
        return False
    except:
        return False


def lost(request):
    if request.method == 'POST':
        if 'send_reset' in request.POST:
            try:
                user = User.objects.get( email = request.POST['email'])
                print 'USER: {0}'.format(user)
            except:
                print 'No user for this email: {0}'
                return render(request, 'authenticate/lost_password.html', {'user_dont_exist':True, 'sent_successful': False})
            email_hash = generate_random_string(40)
            mail_sent = send_mail(
                u'Demande de réinitialisation de mot de passe.',
                u'Veuillez svp clicker sur ce lien pour reinitialiser votre mots de passe.:\nhttp://jeprendsjedonne.com/user/lost?user={0}&hash={1}'.format(user.id, email_hash),
                u'confirm@jeprendsjedonne.com',
                [user.email],
                fail_silently=False
            )
            if mail_sent == 1:
                sent_successful = True
            else:
                sent_successful = False
            user_profile = Profile.objects.get( user_id = user )
            print user_profile
            print 'CITY: {0}'.format(user_profile.city)
            user_profile.email_confirm_hash = email_hash
            user_profile.save()
            return render(request, 'authenticate/lost_password.html', {'sent_successful': sent_successful})
        elif 'new_password' in request.POST:
            errors = {}
            password_not_match = '<span class="color-red">Non Identique</span>'
            password_too_short = '<span class="color-red">8 Caracteres Requis</span>'
            next = request.POST['next']
            user = User.objects.get( id = request.POST['user_id'] )
            hash = request.POST['hash']
            password = request.POST['password']
            confirm_password = request.POST['confirm_password']
            if password == confirm_password:
                if len(password) < 8:
                    errors['password'] = password_too_short
                    return render(request, 'authenticate/lost_password.html', {'set_password': True, 'hash': hash, 'user_id': user.id, 'errors' : errors})
                else:
                    user.set_password(password)
                    user.save()
                    user_profile = Profile.objects.get( user_id = user)
                    user_profile.email_confirm_hash = ''
                    user_profile.save()
                    return HttpResponseRedirect(next)
            else:
                errors['password'] = password_not_match
                return render(request, 'authenticate/lost_password.html', {'set_password': True, 'hash': hash, 'user_id': user.id, 'errors' : errors})
    elif request.method == 'GET':
        if 'user' in request.GET:
            user_profile = Profile.objects.get( user_id_id = request.GET['user'])
            if user_profile.email_confirm_hash == request.GET['hash']:
                return render(request, 'authenticate/lost_password.html', {'set_password': True, 'hash': request.GET['hash'], 'user_id': request.GET['user']})
        
    return render(request, 'authenticate/lost_password.html')

def generate_random_string(length, stringset=string.ascii_letters+string.digits):
    '''
    Returns a string with `length` characters chosen from `stringset`
    >>> len(generate_random_string(20) == 20 
    '''
    return ''.join([stringset[i%len(stringset)] \
        for i in [ord(x) for x in os.urandom(length)]])