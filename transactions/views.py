# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, HttpResponse
from transactions.models import Transaction
from user_profile.models import Profile
from user_profile.views import get_profile

def index(request):
    return render(
        request,
        'transactions/index.html',
    )

@csrf_exempt
def PayPal_IPN(request):
    if request.POST:
        #Refunded
        data = request.POST
        btn_id = data['btn_id']
        user_id = data['custom']
        last_name = data['last_name']
        first_name = data['first_name']
        email = data['payer_email']
        status = data['payment_status']
        if 'option_selection1' in data:
            transactions = 2
        elif 'option_selection2' in data:
            transactions = 5
        elif 'option_selection3' in data:
            transactions = 10
            
    return render(
        request,
        'transactions/index.html'
    )

def list(request):
    user = request.user
    profile = get_profile(request.user.id)
    transactions = Transaction.objects.filter( user = user ).order_by('created')
    return render(
        request,
        'transactions/user_transactions.html',
        {'transactions': transactions, 'profile': profile}
    )

@csrf_exempt
def success(request):
    return render( request, 'transactions/success.html' )

@csrf_exempt    
def cancel(request):
    return render( request, 'transactions/cancel.html' )

