from django.conf.urls import include, url

from transactions import views

urlpatterns = [
    url(r'^success/$', views.success, name='success'),
    url(r'^cancel/$', views.cancel, name='cancel'),
    url(r'^list/$', views.list, name='cancel'),
    url(r'^PayPal_IPN$', include('paypal.standard.ipn.urls')),
    url(r'^$', views.index, name='index'),
]