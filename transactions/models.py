from django.db import models
from django.contrib.auth.models import User
from items.models import Item, ItemDelivery, ItemReservation

class Transaction(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, db_column='user_id', blank = True, default = None )
    item = models.ForeignKey(Item, db_column='item_id', blank = True, default = None, null = True )
    reservation = models.ForeignKey(ItemReservation, db_column='item_reservation_id', null = True, blank = True, default = None )
    delivery = models.ForeignKey(ItemDelivery, db_column='item_delivery_id', null = True, blank = True, default = None )
    credit = models.IntegerField( blank = True, default = None, null = True )
    total_credit = models.IntegerField( blank = True, default = 0 )
    unavaible_credits = models.IntegerField( blank = True, default = 0 )
    transaction = models.IntegerField( blank = True, default = 0 )
    total_transaction = models.IntegerField( blank = True, default = 0 )
    amount = models.IntegerField( blank = True, default = 0 ) 
    type = models.CharField(max_length=50)
    created = models.DateTimeField( auto_now = False, auto_now_add = True )