from django.db import models

class ItemStatus(models.Model):
    
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=25)
    
    class Meta:
        db_table = 'item_status'
        
    def __unicode__(self):              # __unicode__ on Python 2
        return self.name