from django.contrib import admin

from item_status.models import ItemStatus

admin.site.register(ItemStatus)