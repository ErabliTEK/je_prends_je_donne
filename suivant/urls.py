from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = [
    # Examples:
    url(r'^$', 'home.views.index', name='home'),
    url(r'^reglements', 'home.views.reglements', name='reglements'),
    url(r'^termes', 'home.views.termes', name='termes'),
    url(r'^fonctionnement$', 'home.views.fonctionnement', name='fonctionnement'),
    url(r'^child_categories/', include('child_categories.urls', namespace='child')),
    url(r'^parent_categories/', include('parent_categories.urls', namespace='parent')),
    url(r'^items/', include('items.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^user/', include('authenticate.urls', namespace = 'authenticate')),
    url(r'^user_profile/', include( 'user_profile.urls', namespace='user_profile')),
    url(r'^comments/', include( 'comments.urls', namespace='comments')),
    url(r'^messaging/', include( 'messaging.urls', namespace='messaging')),
#    url(r'^transactions/PayPal_IPN', include('paypal.standard.ipn.urls', namespace='PayPal_IPN')),
    url(r'^transactions/', include('transactions.urls', namespace='transactions')),
    url(r'^trasactions/', include('transactions.urls', namespace='transactions')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),
]
