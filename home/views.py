from django.shortcuts import render, render_to_response
from django.contrib.auth.models import User
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from user_profile.views import get_profile

def index(request):
    print 'CLIENT IP ADRESS: {0}'.format(get_client_ip(request))
    data = {}
    user = request.user
    profile = get_profile(request.user.id)
    if user.is_anonymous():
        data['logged'] = 0
    else:
        data['logged'] = 1
        data['first_name'] = user.first_name
        data['last_name'] = user.last_name
    print data
        
    return render(request, "base_generic.html", {'data': data, 'profile': profile} ) 
    
def termes(request):
    return render(request, "terms.html")

def reglements(request):
    return render(request, "reglements.html")

def fonctionnement(request):
    return render(request, "fonctionnement.html")

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

