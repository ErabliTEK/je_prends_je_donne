import os, sys
sys.path.append('/var/www/django')
sys.path.append('/var/www/django/donnez_au_suivant')
os.environ['DJANGO_SETTINGS_MODULE'] = 'donnez_au_suivant.settings'

import django.core.handlers.wsgi

   

application = django.core.handlers.wsgi.WSGIHandler()
