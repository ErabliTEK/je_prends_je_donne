# -*- coding: utf8 -*-
from django.shortcuts import render, render_to_response
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.db.models import Avg
from django.db.models import Q
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.hashers import check_password
from user_profile.forms import edit_profile_form
from user_profile.models import Profile
from items.models import Item, ItemReservation, ItemDelivery
from django.contrib.auth.decorators import login_required
from pilkit.processors import Transpose
from PIL import Image
import StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from pudb import set_trace
from pygeocoder import Geocoder
import phonenumbers
import unicodedata
import urllib2
import time
import json
from django.contrib.auth.decorators import login_required

#set_trace()

@login_required 
def edit(request, status = None, pswd = False):
    data = {}
    items = []
    form = {}
    google_info = {}
    if request.user.is_anonymous():
        data['logged'] = 0
        return HttpResponseRedirect(request, '/login/' )
    profile = get_profile(request.user.id)
    if request.method == 'POST':
        form = edit_profile_form(request.POST, request.FILES)
        # Have we been provided with a valid form?
        if form.is_valid():
            google_info = get_lat_long(form.cleaned_data)
            request.user.first_name = form.cleaned_data['first_name']
            request.user.last_name =  form.cleaned_data['last_name']
            request.user.email = form.cleaned_data['email']
            request.user.save()
            if len(google_info['postal_code']) == 7:
                postal_code = google_info['postal_code']
            elif form.cleaned_data['postal_code']:
                postal_code = form.cleaned_data['postal_code']
            elif google_info['postal_code'] > 0:
                postal_code = google_info['postal_code']
            if google_info['civic_num']:
                civic_num = google_info['civic_num']
            else:
                civic_num = form.cleaned_data['civic_num']
            if 'tel' in form.cleaned_data:
                if form.cleaned_data['tel']:
                    try:
                        parsed_number = phonenumbers.parse(form.cleaned_data['tel'], 'US')
                        form.cleaned_data['tel'] = phonenumbers.format_number(parsed_number, phonenumbers.PhoneNumber())
                    except:
                        form.cleaned_data['tel'] = ''


            if profile['db_data']:  
                profile['db_data'].civic_num = google_info['civic_num']
                profile['db_data'].street = google_info['street']
                profile['db_data'].city = google_info['city']
                profile['db_data'].district = google_info['district']
                profile['db_data'].prov_state = google_info['prov_state']
                profile['db_data'].country = google_info['country']
                profile['db_data'].postal_code = postal_code
                profile['db_data'].tel = form.cleaned_data['tel']
                if 'img' in request.FILES:
                    image = request.FILES['img']
                    image = Image.open(image)
                    output = StringIO.StringIO()
                    image = Transpose().process(image)
                    image.save(output, "JPEG")
                    image = InMemoryUploadedFile(output, None, 'foo.jpg', 'image/jpeg',
                                                      output.len, None)
                    profile['db_data'].img = image
                profile['db_data'].latitude = google_info['latitude']
                profile['db_data'].longitude = google_info['longitude']
                profile['db_data'].save()
                status = 'success'
            else:
                add_db_data = Profile(
                    user_id = request.user,
                    civic_num = google_info['civic_num'],
                    street = google_info['street'],
                    city = google_info['city'],
                    district = google_info['district'],
                    prov_state = google_info['prov_state'],
                    country = google_info['country'],
                    postal_code = postal_code,
                    tel = form.cleaned_data['tel'],
                    latitude = google_info['latitude'],
                    longitude = google_info['longitude'],
                    avaible_credits = 2,
                    unavaible_credits = 0,
                    transactions = 2
                )
                add_db_data.save()
            status = 'success'
        else:
            print 'FORM ERROR: {0}'.format(form.errors)
            status = 'error'
    profile = get_profile(request.user.id)        
    return render(request, 'user_profile/edit_profile.html', { 'form': form, 'profile_data': profile['profile_data'], 'status': status, 'google': google_info, 'pswd': pswd, 'profile': profile })
    
def get_user_quote(user):
#    print 'USER: {0}'.format(user.id)
    taker_obj_rating = None
    user_delivery = None
    user_items = Item.objects.filter( user_id = user )
#    print 'ITEM COUNT: {0}'.format(user_items.count())
    items_reservations = ItemReservation.objects.filter( item__in = user_items )
#    print 'ITEMS RESERVATION COUNT: {0}'.format(items_reservations.count())
    if items_reservations:
        try:
            user_delivery = ItemDelivery.objects.filter( Q(item_reservation__in = items_reservations) & Q( taker_conf = True ) & Q( giver_conf = True ))
#            print 'ITEMS DELIVERY COUNT: {0}'.format(user_delivery.count())
        except:
            user_delivery = None
    if user_delivery:
        try:
            taker_obj_rating = user_delivery.aggregate(Avg('taker_obj_rating'))['taker_obj_rating__avg']
        except:
            taker_obj_rating = None
        try:
            taker_deal_rating = user_delivery.aggregate(Avg('taker_deal_rating'))['taker_deal_rating__avg']
        except:
            taker_deal_rating = None
    
    if taker_obj_rating and taker_deal_rating:
        rating = {}
        rating['avg'] = int((float(taker_obj_rating) + float(taker_deal_rating)) * 10)
        rating['last'] = user_delivery.reverse()[:5]
        rating['total_items'] = user_delivery.count()
    else:
        rating = None
    return rating
    
def get_profile(user_id):
    if not user_id:
        return
    try:
        user_data = User.objects.get( id = user_id )
    except:
        user_data = None
    try:
        db_data = Profile.objects.get( user_id = user_data )
    except:
        db_data = None
    
    if db_data and user_data:
        profile_data = {
            'id': user_data.id,
            'username': user_data.username,
            'first_name': user_data.first_name,
            'last_name': user_data.last_name, 
            'email': user_data.email,
            'civic_num': db_data.civic_num,
            'street': db_data.street,
            'city': db_data.city,
            'district': db_data.district,
            'prov_state': db_data.prov_state,
            'country': db_data.country,
            'postal_code': db_data.postal_code,
            'tel': db_data.tel,
            'avaible_credits': db_data.avaible_credits,
            'unavaible_credits': db_data.unavaible_credits,
            'transactions': db_data.transactions,
            'img': db_data.img,
            'latitude': db_data.latitude,
            'longitude': db_data.longitude,
            'created': db_data.created,
            'modified': db_data.modified
        }
    elif user_data:
        profile_data = {
            'username': user_data.username,
            'first_name': user_data.first_name,
            'last_name': user_data.last_name, 
            'email': user_data.email,
            'civic_num': '',
            'street': '',
            'city': '',
            'district': '',
            'prov_state': '',
            'country': '',
            'postal_code': '',
            'tel': '',
            'avaible_credits': 0,
            'unavaible_credits': 0,
            'transactions': 0,
            'img': '',
            'latitude': '',
            'longitude': '',
            'created': '',
            'modified': ''
        }
    else:
        profile_data = {
            'username': '',
            'first_name': '',
            'last_name': '', 
            'email': '',
            'civic_num': '',
            'street': '',
            'city': '',
            'district': '',
            'prov_state': '',
            'country': '',
            'postal_code': '',
            'tel': '',
            'avaible_credits': 0,
            'unavaible_credits': 0,
            'transactions': 0,
            'img': '',
            'latitude': '',
            'longitude': '',
            'created': '',
            'modified': ''
        }

    return {'profile_data': profile_data, 'db_data': db_data}

@csrf_exempt
def confirm_email(request):
    user = None
    hash = None
    if request.method == 'GET':
        print request.GET
        if 'user' in request.GET:
            user = request.GET['user']
        if 'hash' in request.GET:
            hash = request.GET['hash']
    try:
        profile = Profile.objects.get( user_id_id = user )
        if profile.email_confirm_hash == hash:
            profile.user_id.is_active = 1
            profile.user_id.save()
            profile.delete()
            return HttpResponseRedirect( '/user/login' )
        else:
            return HttpResponseRedirect( '/' )
    except:
        return HttpResponseRedirect( '/' )

def pwd_change(request):
    status = 'error'
    if request.method == 'POST':
        if request.POST['password'] != '' and request.POST['password'] == request.POST['passwordConfirm']:
            if request.POST['passwordCurrent'] and check_password( request.POST['passwordCurrent'], request.user.password ):
                request.user.set_password(request.POST['password'])
                request.user.save()
                status = 'success'
    request.method = None
    request.POST = None
    return edit(request, status, pswd = True)
    
def get_lat_long( profile ):
    address = ''
    if profile['civic_num']:
        address += str(profile['civic_num']) + ', '
    if profile['street']:
        profile['street'] = unicodedata.normalize('NFKD', profile['street']).encode('ASCII', 'ignore')
        address += str(profile['street']) + ', '
    if profile['city']:
        profile['city'] = unicodedata.normalize('NFKD', profile['city']).encode('ASCII', 'ignore')
        address += profile['city'] + ', '
    if profile['prov_state']:
        profile['prov_state'] = unicodedata.normalize('NFKD', profile['prov_state']).encode('ASCII', 'ignore')
        address += profile['prov_state'] + ', '
    if profile['postal_code']:
        address += profile['postal_code'] + ', '
    if profile['country']:
        profile['country'] = unicodedata.normalize('NFKD', profile['country']).encode('ASCII', 'ignore')
        address += profile['country'] + ' '
    if address[0] in " \n\r\t":
        address = address[1:]
    if address[-1] in " \n\r\t":
        address = address[:-1]
    print 'Adresse: {0}'.format(address)
    data = Geocoder.geocode(address)
    
    if data.street_number:
        civic_num = data.street_number
    else:
        civic_num = str(profile['civic_num'])
        
    if data.route:        
        street = data.route
    else:
        street = profile['street']
        
    if data.city:
        city = data.city
    else:
        city = unicodedata.normalize('NFKD', profile['city']).encode('ASCII', 'ignore')
        
    if data.sublocality:
        district = data.sublocality
    else:
        district = ''
        
    if data.administrative_area_level_1:
        prov_state = data.administrative_area_level_1
    else:
        prov_state = unicodedata.normalize('NFKD', profile['prov_state']).encode('ASCII', 'ignore')
        
    if data.country:
        country = data.country
    else:
        country = unicodedata.normalize('NFKD', profile['country']).encode('ASCII', 'ignore')
        
    if data.postal_code:
        postal_code = data.postal_code
    else:
        postal_code = profile['postal_code']
        
    latitude = data.coordinates[0]
    longitude = data.coordinates[1]
    data = {'civic_num': civic_num, 'street': street, 'city': city, 'district': district, 'prov_state': prov_state, 'country': country, 'postal_code': postal_code, 'latitude': latitude, 'longitude': longitude }
    print 'GOOGLE: {0}'.format(data)
    return data    
