from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = [
    # Examples:
    url(r'^confirm/$', 'user_profile.views.confirm_email', name='confirm_email'),
    url(r'^pwd/$', 'user_profile.views.pwd_change', name='password_change'),
    url(r'^$', 'user_profile.views.edit', name='edit_profile'),
]