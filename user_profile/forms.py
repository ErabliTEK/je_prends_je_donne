import pudb
from django import forms    

  
class edit_profile_form(forms.Form):
    first_name = forms.CharField(label = 'Titre: Prenom:', widget=forms.TextInput(attrs={'class':'form-control margin-bottom-20"'}))
    last_name = forms.CharField(label = 'Titre: Nom:', widget=forms.TextInput(attrs={'class':'form-control margin-bottom-20"'}))
    email = forms.CharField(label = 'Titre: Courriel:', widget=forms.TextInput(attrs={'class':'form-control margin-bottom-20"'}))
    civic_num = forms.IntegerField( label = '# Civic:', widget = forms.TextInput( attrs = { 'class':'form-control margin-bottom-20"' }))
    street = forms.CharField(label = 'Titre: Adress:', widget=forms.TextInput(attrs={'class':'form-control margin-bottom-20"'}))
    city = forms.CharField(label = 'Ville:', widget= forms.TextInput( attrs = { 'class':'form-control margin-bottom-20"' }))
    prov_state = forms.CharField(label = 'Province:', widget=forms.Textarea(attrs={'class':'form-control margin-bottom-20"'}))
    country = forms.CharField(label = 'Pays:', widget=forms.Textarea(attrs={'class':'form-control margin-bottom-20"'}))
    postal_code = forms.CharField( required=False, label = 'Code Postal:', widget=forms.Textarea(attrs={'class':'form-control margin-bottom-20"'}))
    tel = forms.CharField( required=False, label = '# Civic:', widget = forms.TextInput( attrs = { 'class':'form-control margin-bottom-20"' }), initial = '')
    img = forms.FileField( required=False, label = 'Photo:', widget=forms.FileInput(attrs={'class':'form-control margin-bottom-20"'}), initial = '')