# -*- coding: utf8 -*-
from django.contrib.auth.models import User
from django.db import models
from paypal.standard.ipn.signals import payment_was_successful, payment_was_reversed, payment_was_refunded, payment_was_flagged
from stdimage import StdImageField
from transactions.models import Transaction
        
class Profile(models.Model):
    user_id = models.OneToOneField(User,
                                primary_key=True,
                                unique=True,
                                db_column="user_id")
    civic_num = models.IntegerField(null = True)
    street = models.CharField(max_length=25, null = True)
    city = models.CharField(max_length=25, null = True)
    district = models.CharField(max_length=40, null = True, blank = True)
    prov_state = models.CharField(max_length=25, null = True)
    country = models.CharField(max_length=25, null = True)
    postal_code = models.CharField(max_length=10, null = True)
    tel = models.CharField(max_length=25, null = True, blank = True )
    avaible_credits = models.IntegerField(null = True)
    unavaible_credits = models.IntegerField(null = True) 
    transactions = models.IntegerField(null = True)
    img = StdImageField(
                    upload_to = 'profile_images/',
                    blank = True, variations={
                            'profile': (300, 250),
                            'thumbnail': (150, 125)
                            }
                    )
    latitude = models.FloatField( null = True)
    longitude = models.FloatField( null = True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    email_confirm_hash = models.CharField(max_length=40, null = True, blank = True)
    class Meta:
        db_table = 'user_profile'

    def __unicode__(self):              # __unicode__ on Python 2
        return u'{0} {1}'.format(self.user_id.first_name, self.user_id.last_name)
    
    
def payment_successful(sender, **kwargs):
    ipn_obj = sender
    # You need to check 'payment_status' of the IPN
    print 'Payment Status: {0}'.format(ipn_obj.payment_status)
    print 'User ID: {0}'.format(ipn_obj.custom)
    print 'Added Transaction: {0}'.format(ipn_obj.transaction_subject)
    if ipn_obj.payment_status == "Completed":
        print 'UPDATING PROFILE'
        profile = Profile.objects.get( user_id_id = ipn_obj.custom )
        if profile:
            print 'Avaible Credit: {0}'.format(profile.avaible_credits)
            if ipn_obj.mc_gross == 1.00:
                num_transactions = 2
            elif ipn_obj.mc_gross == 2.00:
                num_transactions = 5
            elif ipn_obj.mc_gross == 3.00:
                num_transactions = 10
            if num_transactions:
                print 'ADDED TRANSACTIONS: {0}'.format(num_transactions)
                profile.transactions += int(num_transactions)
                profile.save()
                print 'Adding log'
                print User.objects.get( pk = ipn_obj.custom )
                print profile.avaible_credits
                print profile.unavaible_credits
                print num_transactions
                print profile.transactions
                print ipn_obj.mc_gross
                
                transaction_log = Transaction(
                    user = User.objects.get( pk = ipn_obj.custom ),
                    total_credit = profile.avaible_credits,
                    unavaible_credits = profile.unavaible_credits,
                    transaction = num_transactions,
                    total_transaction = profile.transactions,
                    amount = ipn_obj.mc_gross,
                    type = 'Transaction',
                )
                transaction_log.save()
                print 'LOG SAVED'
    else:
        print 'NOT PROCESSED'

    
def payment_reversed(sender, **kwargs):
    ipn_obj = sender
    print 'Reversed'
    print ipn_obj
    return

def payment_flagged(sender, **kwargs):
    ipn_obj = sender
    print 'Flagged'
    print ipn_obj
    return

def payment_refunded(sender, **kwargs):
    ipn_obj = sender
    print 'Refunded'
    print ipn_obj
    print 'ID {0}'.format(ipn_obj.custom)
    return

payment_was_successful.connect(payment_successful)
payment_was_reversed.connect(payment_reversed)
payment_was_flagged.connect(payment_flagged)
payment_was_refunded.connect(payment_refunded)