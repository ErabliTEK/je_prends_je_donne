from django.core.serializers.json import Serializer 
from django.utils.encoding import smart_text
import pudb
import json
import pudb
import json

class CakeSerializer(Serializer):

    def get_dump_object(self, obj):

        self._current['id'] = smart_text(obj._get_pk_val(), strings_only=True)
        data = {
            type(obj).__name__ : self._current
        }

        # fields = obj._meta.get_all_field_names() 
        # for field in fields:
        #     value = getattr(obj, field)
        #     if( type(value).__name__ is 'RelatedManager' ): 
        #         data[value.model.__name__] = []
        #         for related in value.all():
        #             data[value.model.__name__].append( self.get_array_object(related) )
        
        return data

    def get_array_object(self, obj):
        lst = {}
        fields = obj._meta.get_all_field_names() 

        for field in fields:
            value = getattr(obj, field)
            if not ( type(value).__name__ is 'RelatedManager' ): 
                lst[field] = str(value)

        return lst
