from django.db import models

class ChildCategorie(models.Model):
    
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    parent_category_id = models.ForeignKey('parent_categories.ParentCategorie', related_name='parent_category', db_column="parent_category_id")
    
    class Meta:
        db_table = 'child_categories'

    def __unicode__(self):              # __unicode__ on Python 2
        return self.name