from django.contrib import admin

from child_categories.models import ChildCategorie

admin.site.register(ChildCategorie)