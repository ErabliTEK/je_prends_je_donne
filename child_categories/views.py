from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from child_categories.models import ChildCategorie
from django.views.decorators.csrf import csrf_exempt
from cakeserializer import CakeSerializer
import json
serializer = CakeSerializer()

@csrf_exempt
def get_child(request):
    if request.method == 'POST':
        response = request.POST
        child_data = ChildCategorie.objects.filter(parent_category_id = request.POST['category_id'])
        child_data = serializer.serialize(child_data)
    else:
        response = []
    return HttpResponse( child_data, content_type="application/json" )