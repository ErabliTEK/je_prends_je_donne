from django.conf.urls import url
from child_categories import views

urlpatterns = [
    url(r'^get_child/$', views.get_child, name='get_child'),
#    url(r'^$', views.index, name='index'),
]