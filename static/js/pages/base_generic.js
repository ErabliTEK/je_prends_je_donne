var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

//Footer Nouveauté
function get_latest_items(){
    $.ajax({
        url: "/items/latest",
        success: function(response, status){
            console.log('Status: ' + status)
            if ( status == "success"){
                console.log('Reussi');
                $('#nouveaute').html(response);
            }
            else {
                console.log(response)
            }
        }
    });
}

get_latest_items()