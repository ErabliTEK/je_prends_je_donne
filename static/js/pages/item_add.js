getChildCategories($("#id_parent_category_id").val());

$("#id_parent_category_id").change(function(){
    getChildCategories($(this).val());
})

function getChildCategories( category_id ){
    $.ajax({
        type: "POST",
        url: "/child_categories/get_child/",
        data: { "category_id": category_id },
        dataType: "json",
        success: function(response, status){
            if (!response.success){
                console.log('Failed to get readings');
            }
            console.log(response)
            setChildCategories( response )
        }
    });
}

function setChildCategories( childCategories ){
    $("#id_child_category_id").html("");
    $("#id_child_category_id").append('<option value=""> Veuillez sélectionner une sous catégorie.</option>');
    child = $('[data-id]').data('id')
    childCategories.forEach(function( childCategorie ){
        if( child == childCategorie['ChildCategorie']['id'] ){
            $("#id_child_category_id").append('<option selected="selected" value="' + childCategorie['ChildCategorie']['id'] + '">' + childCategorie['ChildCategorie']['name'] + '</option>');
        }else{
            $("#id_child_category_id").append('<option value="' + childCategorie['ChildCategorie']['id'] + '">' + childCategorie['ChildCategorie']['name'] + '</option>');
        }
    })
    
}