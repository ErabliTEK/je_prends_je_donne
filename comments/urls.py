from django.conf.urls import url

from comments import views as comments_views
from home import views as home_views

urlpatterns = [
    url(r'^post_comment/$', comments_views.post_comment, name='post_comment'),
    url(r'^$', home_views.index, name='index'),
]