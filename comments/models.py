from django.db import models
from django.contrib.auth.models import User
from items.models import Item

class Comment(models.Model):
    
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User, related_name='user', db_column='user_id')
    item_id = models.ForeignKey(Item, related_name='comments', db_column='item_id')
    comment = models.TextField()
    viewed = models.BooleanField( default = False )
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
        
    class Meta:
        db_table = 'items_comments'
        
    def __unicode__(self):              # __unicode__ on Python 2
        return self.comment