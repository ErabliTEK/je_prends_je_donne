from django.shortcuts import render, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from items.models import Item, MyAdAlert
from comments.models import Comment
from django.contrib.auth.decorators import login_required
from user_profile.views import get_profile
from pudb import set_trace

#set_trace()


def list_comments( item_id ):
    comments_list = []
    try:
        comments = Comment.objects.filter( item_id = item_id ).reverse()
    except:
        comments = None
    if comments:
        for x in comments:
            comments_list.append(
                {
#                    'user_id':x.user_id,
                    'item_id':x.item_id,
                    'comment':x.comment
                }
            )
    return comments_list
    
    
def post_comment(request):
    if request.POST:
        print request.POST
        item = Item.objects.get( id = request.POST['item_id'] )
        comment = Comment( item_id = item, user_id = request.user, comment = request.POST['comment'] )    
        comment.save()
        my_ad_alert = MyAdAlert( user = item.user_id, item = item )
        my_ad_alert.save()
        return HttpResponseRedirect('/items/details/' + request.POST['item_id'])
        
    return HttpResponseRedirect('/items/')