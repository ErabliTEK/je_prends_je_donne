from django.contrib.auth.models import User
from django.db import models
        
class MessageState(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=25)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    
    class Meta:
        db_table = 'message_states'

    def __unicode__(self):              # __unicode__ on Python 2
        return self.name

        
        
        
class Message(models.Model):
    id = models.AutoField(primary_key=True)
    sent_from = models.ForeignKey(User, related_name='sent_from', db_column='sent_from')
    sent_to = models.ForeignKey(User, related_name='sent_to', db_column='sent_to')
    message = models.TextField()
    state = models.ForeignKey(MessageState, related_name='MessageState', db_column='state')
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    
    class Meta:
        db_table = 'user_messages'

    def __unicode__(self):              # __unicode__ on Python 2
        return 'To: {0}, From: {1}'.format( self.sent_from, self.sent_to )