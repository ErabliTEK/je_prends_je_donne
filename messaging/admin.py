from django.contrib import admin

from messaging.models import MessageState, Message

admin.site.register(MessageState)
admin.site.register(Message)