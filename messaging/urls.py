from django.conf.urls import url
from messaging import views

urlpatterns = [
    url(r'^num_messages$', views.num_messages, name='num_messages'),
    url(r'^get_user/$', views.get_user, name='get_user'),
    url(r'^pm/$', views.pm, name='pm'),
    url(r'^sub_menu$', views.sub_menu, name='sub_menu'),
    url(r'^list_users/$', views.list_users, name='sub_menu'),
#    url(r'^$', views.message, name='index'),
]