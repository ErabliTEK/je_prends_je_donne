from django.shortcuts import render, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from cakeserializer import CakeSerializer
from django.contrib.auth.decorators import login_required
from user_profile.views import get_profile
from messaging.models import Message, MessageState
from urlparse import urlparse
from pudb import set_trace
import json
serializer = CakeSerializer()

#set_trace()

def list_users(request):
    data = []
    userlist = User.objects.all().order_by('username')
    for x in userlist:
        data.append({'username':x.username, 'id':x.id})
    return HttpResponse( data, content_type="application/json" )

def num_messages(request):
    messages = None
    messages = Message.objects.filter( Q(sent_to = request.user.id) & Q( state = MessageState.objects.get(name = 'Unread') ))
    lenght = len(messages)
    if lenght == 0:
        return HttpResponse()
    print lenght
    return HttpResponse(lenght)

@csrf_exempt
def get_user(request):
    user_list = []
    if request.POST:
        typed = request.POST['typed']
        print typed
        response = User.objects.filter( Q( username__contains = typed ) | Q( first_name__contains = typed ) | Q( last_name__contains = typed ) )
        for user in response:
            user_list.append({'username':user.username})
    #user_list = { 'user_list': user_list } 
    print user_list
    
    return HttpResponse( user_list, content_type="application/json" )

 
@csrf_exempt    
def pm(request):
    messages = []

    
    if 'pmmessage' in request.POST and 'from' in request.GET:
        print request.GET['from']
        print request.POST['pmmessage']
        if request.POST['pmmessage'] != '':
            pm_message = Message( sent_from = request.user, sent_to = User.objects.get(id = request.GET['from']), message = request.POST['pmmessage'], state = MessageState.objects.get( id = 1 ) )
            pm_message.save()
        return HttpResponse( json.dumps({ 'success': True }), content_type="application/json" )
        
    if 'from' in request.GET and 'refresh' in request.GET:
        messages = Message.objects.filter( ( Q( sent_to = request.user.id ) & Q( sent_from = request.GET['from'] ) ) | ( Q( sent_to = request.GET['from'] ) & Q( sent_from = request.user.id ) )).order_by('-id')[:25]
        messages = reversed(messages)
        Message.objects.filter( ( Q( sent_to = request.user.id ) & Q( sent_from = request.GET['from'] ) ) ).update( state = 2 )
        return render( request, 'messaging/generate_pm.html', { 'messages': messages, 'from': User.objects.get(id=request.GET['from']) } )
    elif 'from' in request.GET:
        return render( request, 'messaging/pm.html', { 'from': User.objects.get(id=request.GET['from']) } )
    

def sub_menu(request):
    messages = Message.objects.filter( Q(sent_to = request.user.id) & Q(state = 1) )
    sent_from = []
    data = []
    users = []
    for message in messages:
        if not message.sent_from in sent_from:
            sent_from.append( message.sent_from )
    for x in sent_from:
        num = messages.filter( sent_from = x )
        data.append({ 'from': x, 'new_message': len(num)})
    userlist = User.objects.all().order_by('username')
    for x in userlist:
        if x.id != request.user.id:
            users.append({'username':x.username, 'id':x.id})
#    return HttpResponse( data, content_type="application/json" )
    return render( request, 'messaging/sub_menu.html', {'data':data, 'users':users} )