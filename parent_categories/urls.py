from django.conf.urls import url
from parent_categories import views

urlpatterns = [
    url(r'^get_parent/$', views.get_parent, name='get_parent'),
#    url(r'^$', views.index, name='index'),
]