from django.db import models

class ParentCategorie(models.Model):
    
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    
    class Meta:
        db_table = 'parent_categories'

    def __unicode__(self):              # __unicode__ on Python 2
        return self.name