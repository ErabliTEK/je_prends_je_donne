from __future__ import division
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from parent_categories.models import ParentCategorie
from child_categories.models import ChildCategorie
from items.models import Item
from django.views.decorators.csrf import csrf_exempt
from user_profile.views import get_profile
from user_profile.models import Profile
from django.contrib.auth.models import User
from django.db.models import Q
import math


@csrf_exempt
def get_parent(request):
    user_profile = get_profile(request.user.id)
    parents = ParentCategorie.objects.all()
    total = 0
    childs_list = []
    parent_list = []
    if 'max_distance' in request.GET:
        max_distance = int(request.GET['max_distance'])
    else:
        max_distance = 25
        
    if user_profile:
        min_max = min_max_lat_lon(user_profile['profile_data']['latitude'], user_profile['profile_data']['longitude'], max_distance)
        users = User.objects.filter( Q(profile__latitude__lte=min_max['lat_max']) & Q(profile__latitude__gte=min_max['lat_min']) & Q(profile__longitude__lte=min_max['lng_max']) & Q(profile__longitude__gte=min_max['lng_min'])  )
        items = Item.objects.filter( user_id__in=users)
    else:
        items = Item.objects.all()
    for parent in parents:
        childs_list[:] = []
        qts_items = 0
        childs = None
        childs = ChildCategorie.objects.filter( parent_category_id = parent )
        for child in childs:
            filtered_items = items.filter( Q( child_category_id = child ) & ( Q( status_id = 1 ) | Q( status_id = 2 ) ) )
            qts_items = filtered_items.count()
            childs_list.append( { 'child': child, 'qts_items': qts_items } )  
        parent_list.append( { 'parent': parent, 'childs': list(childs_list) } )
        total += len(childs_list) + 1  
    #return
    total_per_row = total / 3    
    
    temp = []
    row_1 = []
    total_left = total_per_row
    row_1.append(parent_list.pop(0))
    total_left -= ( len(row_1[0]['childs']) + 2 )
    key = len(parent_list) - 1
    for item in reversed(parent_list):
        if 'childs' in temp:
            if len(item['childs']) <= total_left + 2 and len(item['childs']) > len(temp['childs']):
                parent_list.append(temp)
                total_left += len(temp) + 2
                key += 1
                temp = parent_list.pop(key)
                total_left -= len(temp) + 2
                key -= 1
        else:
            temp = parent_list.pop(key)
            total_left -= len(temp) + 2
            key -= 1
   
    row_1.append(temp)
    
    row_2 = []
    total_left = total_per_row
    row_2.append(parent_list.pop(0))
    total_left -= ( len(row_2[0]['childs']) + 1 )
    for x, item in enumerate(parent_list):
        if len(item['childs']) <= total_left + 2:
            total_left -= len(item['childs'])
            total_left -= 1
            row_2.append(parent_list.pop(x))       
    
    
    row_3 = parent_list
      
        

    
    return render(request, 'parent_categories/index.html', { 'row_1': row_1, 'row_2': row_2, 'row_3': row_3, 'profile': user_profile, 'max_distance': max_distance } )

def min_max_lat_lon(lat, lng, radius):
    lng_min = lng - radius / abs(math.cos(math.radians(lat)) * 111.044736)
    lng_max = lng + radius / abs(math.cos(math.radians(lat)) * 111.044736)
    lat_min = lat - (radius / 111.044736)
    lat_max = lat + (radius / 111.044736)
    return  {'lat_min': lat_min, 'lat_max': lat_max, 'lng_min': lng_min, 'lng_max': lng_max}

def distance_func(lat1, lon1, lat2, lon2):
    R = 6371000
    x = (math.radians(lon2) - math.radians(lon1)) * math.cos( 0.5*(math.radians(lat2)+math.radians(lat1)) )
    y = math.radians(lat2) - math.radians(lat1)
    d = R * math.sqrt( x*x + y*y )
    return int(d / 1000)